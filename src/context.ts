import type {IncomingMessage, OutgoingHttpHeaders} from "http";
import type {RouteParameters} from "./route";

export type Context = {
    readonly request: IncomingMessage;
    readonly response: {
        body: any | null;
        headers: OutgoingHttpHeaders;
        statusCode: number;
        statusMessage: string;
    };
    routeParameters: RouteParameters;
};