import typescript from "@rollup/plugin-typescript";

/**
 * @type {Array<import('rollup').RollupOptions>}
 */
const configurations = [
    {
        input: 'src/index.ts',
        plugins: [
            typescript({})
        ],
        output: {
            file: 'dist/index.cjs',
            format: "commonjs"
        }
    },
    {
        input: 'src/index.ts',
        plugins: [
            typescript({
                target: "es2015"
            })
        ],
        output: {
            file: 'dist/index.mjs',
            format: "esm"
        }
    }
];

export default configurations;