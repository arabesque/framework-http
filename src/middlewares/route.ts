import type {RouteHandler} from "../route";
import type {Middleware} from "@arabesque/core";
import type {Context} from "../context";

export const executeRouteHandler = (
    routeHandler: RouteHandler<any>
): Middleware<Context> => {
    return (context, next) => {
        return routeHandler(context.routeParameters, context.request)
            .then((result) => {
                context.response.body = result;

                return next(context);
            });
    };
};