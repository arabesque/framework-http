import typescript from "@rollup/plugin-typescript";

/**
 * @type {Array<import('rollup').RollupOptions>}
 */
const configurations = [
    {
        input: 'test/index.ts',
        plugins: [
            typescript({
                tsconfig: './test/tsconfig.json',
                module: "es2015"
            })
        ],
        output: {
            file: 'dist/index.mjs',
            sourcemap: true
        }
    }
];

export default configurations;