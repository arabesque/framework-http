import type {Listener} from "@arabesque/core";
import type {Context} from "./context";
import {Server} from "http";
import {Readable} from "stream";
import {createListener as createArabesqueListener} from "@arabesque/listener-http";

export const createListener = (
    server: Server
): Listener<number, Context> => {
    return (
        channel, handler
    ) => {
        const listener = createArabesqueListener(
            server
        );

        return listener(channel, (serverContext) => {
            const {message, response: serverResponse} = serverContext;

            return handler({
                request: message,
                response: {
                    body: null,
                    headers: serverResponse.getHeaders(),
                    statusCode: serverResponse.statusCode,
                    statusMessage: serverResponse.statusMessage
                },
                routeParameters: {}
            }).then((context) => {
                return new Promise((resolve) => {
                    const {response} = context;
                    const {body, headers, statusCode, statusMessage} = response;

                    serverResponse.statusCode = statusCode;
                    serverResponse.statusMessage = statusMessage;

                    for (const [name, value] of Object.entries(headers)) {
                        if (value) {
                            serverResponse.setHeader(name, value);
                        }
                    }

                    const end = () => {
                        serverResponse.end(() => resolve(serverContext));
                    }

                    if (body === null) {
                        end();
                    } else {
                        if (Readable.isReadable(body)) {
                            body.pipe(serverResponse);

                            serverResponse.on("close", end);
                        } else {
                            serverResponse.write(body, end);
                        }
                    }
                });
            });
        });
    };
}
