import type {Middleware} from "@arabesque/core";
import type {Context} from "./context";
import {createANDMiddleware} from "@arabesque/logic-middlewares";
import {matchMethod, matchUrlSpecification} from "./middlewares/request";
import type {HTTPMethod} from "./request";

export type RouteParameters = Record<string, string>;

export type RouteHandler<RP extends RouteParameters, R = any> = (
    parameters: RP,
    request: Context["request"]
) => Promise<R>;

export const createRoute = (
    method: HTTPMethod,
    specification: string,
    handler: Middleware<Context>
): Middleware<Context> => {
    return createANDMiddleware(
        matchMethod(method),
        matchUrlSpecification(specification),
        handler
    );
};