import type {Middleware} from "@arabesque/core";
import type {Context} from "../context";
import {METHODS} from "http";
import type {RouteParameters} from "../route";
import {extractParameters, HTTPMethod} from "../request";

const isAnHTTPMethod = (candidate: string): candidate is HTTPMethod => {
    return METHODS.includes(candidate);
};

export const matchUrlSpecification = <P extends RouteParameters>(
    specification: string
): Middleware<Context> => (context, next) => {
    const {request} = context;

    if (request.url !== undefined) {
        const parameters = extractParameters<P>(request, specification);

        if (parameters !== null) {
            context.routeParameters = parameters;

            return next(context);
        }
    }

    return Promise.resolve(context);
};

export const matchMethod = (
    method: HTTPMethod
): Middleware<Context> => (context, next) => {
    const {request} = context;

    if (request.method && isAnHTTPMethod(request.method) && (request.method === method)) {
        return next(context);
    }

    return Promise.resolve(context);
};
