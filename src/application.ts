import {createApplication as createCoreApplication, Middleware} from "@arabesque/core";
import {createListener} from "./listener";
import type {Context} from "./context";
import {createServer} from "http";

export const createApplication = (
    middleware: Middleware<Context>
) => {
    const server = createServer();

    return {
        start: (port: number) => {
            const listener = createListener(server);
            const application = createCoreApplication(listener, middleware);

            return application(port)
                .then(() => {
                    console.info(`Application started, listening to TCP port ${port}`);
                });
        },
        server
    };
}